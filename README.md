# Formatteren van Belgische telefoonnummers met REGEX

**Author:** Ingabire Marie-Aimée

**Course:**  AD5

**AJ:**  2015-2016

**Repository:** [AD5-RegexWalkthrough](https://bitbucket.org/neit1o/ad5-regexwalkthrough)

## Publiek

Gevorderd

## Onderdelen
1. Voorbereiding
2. Soort achterhalen
3. Telefoonnummer schoonmaken
4. Telefoonnummers van dorpen en kleine steden
5. Testen
6. Mogelijke uitbreidingen

## Vereisten
De walkthrough is gemaakt en getest op Visual Studio 2015 (Ultimate edition). Het kan zijn dat sommige operaties niet werken op andere versies.


### Walkthrough

De walkthrough zal voor de formatering de regels volgen die op TaalAdvies zijn.
Link: [Telefoonnummers in België (algemeen)](http://taaladvies.net/taal/advies/tekst/52/)

### Voorbereiding

Om te beginnen zal je een database moeten opstellen. Voor deze walkthrough hebben zullen we ons houden aan locale databanken.
Vooraleer je hieraan kunnen beginnen moeten je eerste een project aanmaken.

Ga naar `File > New > Project`. Kies dan voor een **Console Application**, noem het *NummersMetREGEX*. De naam maakt opzich niet uit.

### Soort telefoonnummer achterhalen

Vooraleer je de nummers kunnen schoonmaken, moeten je eerst weten welke soort nummer het is. Is het een vast, GSM ...

Volledige lijst: [Wikipedia - Telephone numbers in Belgium](https://en.wikipedia.org/wiki/Telephone_numbers_in_Belgium)


Je zal eerst een functie nodig hebben om alle overbodige tekens weg te halen. Deze functie verwijdert punten, spaties en schuine strepen.
```

    public static string CleanPhoneNumber(String phonenumber)
    {
        return Regex.Replace(phonenumber, "\\.|\\/|\\s", "");
    }

```

Je zal ook een enum nodig hebben om verschillende soorten telefoonnummers bij te houden.

```

    public enum PhoneNumberType
    {
        None,
        HomePhone,
        CellPhone,
        International,
        InternationalLong
    }
```

De `CheckType` functie zal d.m.v. regex patronen het soort nummer proberen te achterhalen. 

```

    public static PhoneNumberType CheckType(String phonenumber)
    {
        PhoneNumberType type = PhoneNumberType.None;
        phonenumber = CleanPhoneNumber(phonenumber);

        string patternHomePhone = "^0(?:[0-9]\\s?){8}$";
        string patternCellPhone = "^04(?:[0-9]\\s?){8}$";
        string patternInternational = "^\\+(?:[0-9]\\s?){11}$";
        string patternInternational2 = "^0032(?:[0-9]\\s?){9}$";

        if (Regex.IsMatch(phonenumber, patternHomePhone))
        {
            type = PhoneNumberType.HomePhone;
        }
        else if (Regex.IsMatch(phonenumber, patternCellPhone))
        {
            type = PhoneNumberType.CellPhone;
        }
        else if (Regex.IsMatch(phonenumber, patternInternational))
        {
            type = PhoneNumberType.International;
        }
        else if (Regex.IsMatch(phonenumber, patternInternational2))
        {
            type = PhoneNumberType.InternationalLong;
        }

        return type;
    }
```

### Telefoonnummer schoonmaken

De `FormatPhoneNumber` functie zal het formaat van telefoonnummer aanpassen.

```

    public static string FormatPhoneNumber(String phonenumber)
    {
        String newPhoneNumber = phonenumber;
        Regex regex;
        Match match;
        PhoneNumberType type = CheckType(phonenumber);
        phonenumber = CleanPhoneNumber(phonenumber);

        string patternHomePhone = "^0((?:\\d){1})((?:\\d){3})((?:\\d){2})((?:\\d){2})$";
        string patternCellPhone = "^04((?:\\d){2})((?:\\d){2})((?:\\d){2})((?:\\d){2})$";
        string patternInternational = "^\\+32((?:\\d){3})((?:\\d){2})((?:\\d){2})((?:\\d){2})$";
        string patternInternationalLong = "^0032((?:\\d){3})((?:\\d){2})((?:\\d){2})((?:\\d){2})$";

        switch (type)
        {
            case PhoneNumberType.HomePhone:
                regex = new Regex(patternHomePhone);
                match = regex.Match(phonenumber);
                if (match.Success)
                {
                    newPhoneNumber = $"0{match.Groups[1]} {match.Groups[2]} {match.Groups[3]} {match.Groups[4]}";
                }
                break;
            case PhoneNumberType.CellPhone:
                regex = new Regex(patternCellPhone);
                match = regex.Match(phonenumber);
                if (match.Success)
                {
                    newPhoneNumber = $"04{match.Groups[1]} {match.Groups[2]} {match.Groups[3]} {match.Groups[4]}";
                }
                break;
            case PhoneNumberType.International:
                regex = new Regex(patternInternational);
                match = regex.Match(phonenumber);
                if (match.Success)
                {
                    newPhoneNumber = $"+32 {match.Groups[1]} {match.Groups[2]} {match.Groups[3]} {match.Groups[4]}";
                }
                break;
            case PhoneNumberType.InternationalLong:
                regex = new Regex(patternInternationalLong);
                match = regex.Match(phonenumber);
                if (match.Success)
                {
                    newPhoneNumber = $"+32 {match.Groups[1]} {match.Groups[2]} {match.Groups[3]} {match.Groups[4]}";
                }
                break;
            default:
                break;
        }

        return newPhoneNumber;
    }

```

### Telefoonnummers van dorpen en kleine steden

De huidige versie van de code werkt enkel voor vaste lijnen van grote steden. Het is dat er geen verschil is in aantal tussen de nummer van grote steden (bv. 02 333 10 00) en kleine steden (bv. 016 78 23 76).
Op de wikipedia pagina is er een lijst van netnummers van grote steden.

- 02 voor Brussel
- 03 voor Antwerpen
- 04 voor Luik
- 09 voor Gent

Om te een onderscheid te maken tussen de twee soorten zal je de eerste twee getallen moeten bekijken.

De nieuwe regex zouden er als volgt moeten uitzien:

```
    public static PhoneNumberType CheckType(String phonenumber)
    {
    	...

        string patternHomePhone = "^0(?:[0-9]\\s?){8}$";
        string patternHomePhoneCity = "^0(?:[2|3|4|9]){1}(?:[0-9]){7}$";
        ...

        if (Regex.IsMatch(phonenumber, patternHomePhoneCity))
        {
            type = PhoneNumberType.HomePhoneCity;
        }
        else if (Regex.IsMatch(phonenumber, patternHomePhone))
        {
            type = PhoneNumberType.HomePhone;
        }
        ...
    }

```

```
	public static string FormatPhoneNumber(String phonenumber)
	{
		...

	    string patternHomePhone = "^0((?:\\d){2})((?:\\d){2})((?:\\d){2})((?:\\d){2})$";
	    string patternHomePhoneCity = "^0((?:\\d){1})((?:\\d){3})((?:\\d){2})((?:\\d){2})$";
	    ...

	    switch (type)
	    {
	        case PhoneNumberType.HomePhone:
	            regex = new Regex(patternHomePhone);
	            match = regex.Match(phonenumber);
	            if (match.Success)
	            {
	                newPhoneNumber = $"0{match.Groups[1]} {match.Groups[2]} {match.Groups[3]} {match.Groups[4]}";
	            }
	            break;
	        case PhoneNumberType.HomePhoneCity:
	            regex = new Regex(patternHomePhoneCity);
	            match = regex.Match(phonenumber);
	            if (match.Success)
	            {
	                newPhoneNumber = $"0{match.Groups[1]} {match.Groups[2]} {match.Groups[3]} {match.Groups[4]}";
	            }
	            break;
	        ...
	    }
		...
	}

```
### Testen
Test in de methode *Main* van *Program* alle CRUD operaties:
```
    String phonenumber1 = "0484324876";
    String phonenumber2 = "02336789";
    String phonenumber3 = "023367899";
    String phonenumber4 = "+32484324876";
    String phonenumber5 = "0032484324876";
    String phonenumber6 = "0484.32.48.76";
    String phonenumber7 = "0484324876";
    String phonenumber8 = "023367899";
    String phonenumber9 = "02/336.78.99";
    String phonenumber10 = "016/21 09 00";

    List<String> lstPhoneNumbers = new List<String>();
    lstPhoneNumbers.Add(phonenumber1);
    lstPhoneNumbers.Add(phonenumber2);
    lstPhoneNumbers.Add(phonenumber3);
    lstPhoneNumbers.Add(phonenumber4);
    lstPhoneNumbers.Add(phonenumber5);
    lstPhoneNumbers.Add(phonenumber6);
    lstPhoneNumbers.Add(phonenumber7);
    lstPhoneNumbers.Add(phonenumber8);
    lstPhoneNumbers.Add(phonenumber9);
    lstPhoneNumbers.Add(phonenumber10);

    foreach (var phonenumber in lstPhoneNumbers)
    {
        String newPhonenumber = phonenumber;
        PhoneNumberType type = CheckType(phonenumber);
        newPhonenumber = FormatPhoneNumber(phonenumber);
        Console.WriteLine($"Type: {type}\n\t\tOld: {phonenumber}\n\t\tFormatted: {newPhonenumber}");
    }

    Console.ReadKey();
```

Als alles juist geïmplementeerd werd, krijgt je volgende output te zien:

```

	Type: CellPhone
	                Old: 0484324876
	                Formatted: 0484 32 48 76
	Type: None
	                Old: 02336789
	                Formatted: 02336789
	Type: HomePhoneCity
	                Old: 023367899
	                Formatted: 02 336 78 99
	Type: International
	                Old: +32484324876
	                Formatted: +32 484 32 48 76
	Type: InternationalLong
	                Old: 0032484324876
	                Formatted: +32 484 32 48 76
	Type: CellPhone
	                Old: 0484.32.48.76
	                Formatted: 0484 32 48 76
	Type: CellPhone
	                Old: 0484324876
	                Formatted: 0484 32 48 76
	Type: HomePhoneCity
	                Old: 023367899
	                Formatted: 02 336 78 99
	Type: HomePhoneCity
	                Old: 02/336.78.99
	                Formatted: 02 336 78 99
	Type: HomePhone
	                Old: 016/21 09 00
	                Formatted: 016 21 09 00

```

### Mogelijke uitbreidingen

In deze walkthrough hebben we enkel rekening gehouden met "normale nummers". Op het wikipedia pagina zijn er ook anderen zoals bijvoorbeeld 0800-nummers. Dit zou een goede uitbreiding zijn.
Een nog betere uitbreiding is niet enkel voor België telefoonnummers schoonmaken maar voor alle landen door te werken met `Locales`.
