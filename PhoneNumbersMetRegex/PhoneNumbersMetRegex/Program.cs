﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
// ReSharper disable All

namespace testRegex
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            String phonenumber1 = "0484324876";
            String phonenumber2 = "02336789";
            String phonenumber3 = "023367899";
            String phonenumber4 = "+32484324876";
            String phonenumber5 = "0032484324876";
            String phonenumber6 = "0484.32.48.76";
            String phonenumber7 = "0484324876";
            String phonenumber8 = "023367899";
            String phonenumber9 = "02/336.78.99";
            String phonenumber10 = "016/21 09 00";

            List<String> lstPhoneNumbers = new List<String>();
            lstPhoneNumbers.Add(phonenumber1);
            lstPhoneNumbers.Add(phonenumber2);
            lstPhoneNumbers.Add(phonenumber3);
            lstPhoneNumbers.Add(phonenumber4);
            lstPhoneNumbers.Add(phonenumber5);
            lstPhoneNumbers.Add(phonenumber6);
            lstPhoneNumbers.Add(phonenumber7);
            lstPhoneNumbers.Add(phonenumber8);
            lstPhoneNumbers.Add(phonenumber9);
            lstPhoneNumbers.Add(phonenumber10);

            foreach (var phonenumber in lstPhoneNumbers)
            {
                String newPhonenumber = phonenumber;
                PhoneNumberType type = CheckType(phonenumber);
                newPhonenumber = FormatPhoneNumber(phonenumber);
                Console.WriteLine($"Type: {type}\n\t\tOld: {phonenumber}\n\t\tFormatted: {newPhonenumber}");
            }

            Console.ReadKey();
        }

        public enum PhoneNumberType
        {
            None,
            HomePhone,
            HomePhoneCity,
            CellPhone,
            International,
            InternationalLong
        }
        public static string CleanPhoneNumber(String phonenumber)
        {
            return Regex.Replace(phonenumber, "\\.|\\/|\\s", "");
        }
        public static string FormatPhoneNumber(String phonenumber)
        {
            String newPhoneNumber = phonenumber;
            Regex regex;
            Match match;
            PhoneNumberType type = CheckType(phonenumber);
            phonenumber = CleanPhoneNumber(phonenumber);

            string patternHomePhone = "^0((?:\\d){2})((?:\\d){2})((?:\\d){2})((?:\\d){2})$";
            string patternHomePhoneCity = "^0((?:\\d){1})((?:\\d){3})((?:\\d){2})((?:\\d){2})$";
            string patternCellPhone = "^04((?:\\d){2})((?:\\d){2})((?:\\d){2})((?:\\d){2})$";
            string patternInternational = "^\\+32((?:\\d){3})((?:\\d){2})((?:\\d){2})((?:\\d){2})$";
            string patternInternational2 = "^0032((?:\\d){3})((?:\\d){2})((?:\\d){2})((?:\\d){2})$";

            switch (type)
            {
                case PhoneNumberType.HomePhone:
                    regex = new Regex(patternHomePhone);
                    match = regex.Match(phonenumber);
                    if (match.Success)
                    {
                        newPhoneNumber = $"0{match.Groups[1]} {match.Groups[2]} {match.Groups[3]} {match.Groups[4]}";
                    }
                    break;
                case PhoneNumberType.HomePhoneCity:
                    regex = new Regex(patternHomePhoneCity);
                    match = regex.Match(phonenumber);
                    if (match.Success)
                    {
                        newPhoneNumber = $"0{match.Groups[1]} {match.Groups[2]} {match.Groups[3]} {match.Groups[4]}";
                    }
                    break;
                case PhoneNumberType.CellPhone:
                    regex = new Regex(patternCellPhone);
                    match = regex.Match(phonenumber);
                    if (match.Success)
                    {
                        newPhoneNumber = $"04{match.Groups[1]} {match.Groups[2]} {match.Groups[3]} {match.Groups[4]}";
                    }
                    break;
                case PhoneNumberType.International:
                    regex = new Regex(patternInternational);
                    match = regex.Match(phonenumber);
                    if (match.Success)
                    {
                        newPhoneNumber = $"+32 {match.Groups[1]} {match.Groups[2]} {match.Groups[3]} {match.Groups[4]}";
                    }
                    break;
                case PhoneNumberType.InternationalLong:
                    regex = new Regex(patternInternational2);
                    match = regex.Match(phonenumber);
                    if (match.Success)
                    {
                        newPhoneNumber = $"+32 {match.Groups[1]} {match.Groups[2]} {match.Groups[3]} {match.Groups[4]}";
                    }
                    break;
                default:
                    break;
            }

            return newPhoneNumber;
        }
        public static PhoneNumberType CheckType(String phonenumber)
        {
            PhoneNumberType type = PhoneNumberType.None;
            phonenumber = CleanPhoneNumber(phonenumber);

            string patternHomePhone = "^0(?:[0-9]\\s?){8}$";
            string patternHomePhoneCity = "^0(?:[2|3|4|9]){1}(?:[0-9]){7}$";
            string patternCellPhone = "^04(?:[0-9]\\s?){8}$";
            string patternInternational = "^\\+(?:[0-9]\\s?){11}$";
            string patternInternational2 = "^0032(?:[0-9]\\s?){9}$";

            if (Regex.IsMatch(phonenumber, patternHomePhoneCity))
            {
                type = PhoneNumberType.HomePhoneCity;
            }
            else if (Regex.IsMatch(phonenumber, patternHomePhone))
            {
                type = PhoneNumberType.HomePhone;
            }
            else if (Regex.IsMatch(phonenumber, patternCellPhone))
            {
                type = PhoneNumberType.CellPhone;
            }
            else if (Regex.IsMatch(phonenumber, patternInternational))
            {
                type = PhoneNumberType.International;
            }
            else if (Regex.IsMatch(phonenumber, patternInternational2))
            {
                type = PhoneNumberType.InternationalLong;
            }

            return type;
        }

    }

}
